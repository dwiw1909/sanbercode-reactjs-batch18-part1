// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

let klm = `${kataPertama} ${kataKedua[0].toUpperCase()+kataKedua.slice(1)} ${kataKetiga} ${kataKeempat.toUpperCase()}`
// saya Senang belajar JAVASCRIPT
console.log(klm);


//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

let jml = Number(kataPertama) + Number(kataKedua) + Number(kataKetiga) + Number(kataKeempat)
console.log(jml);


// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


// soal 4
var nilai = 75

if(nilai >= 80){
  console.log('indeksnya A');
} else if(nilai >= 70 && nilai < 80){
  console.log('indexnya B');
} else if(nilai >= 60 && nilai < 70){
  console.log('indexnya C');
} else if(nilai >= 50 && nilai < 60){
  console.log('indexnya D');
} else if(nilai < 50){
  console.log('indexnya E');
}


// soal 5
var tanggal = 19;
var bulan = 9;
var tahun = 1990;
let newBulan;

switch (bulan) {
  case 1: { newBulan = 'Januari'; break; }
  case 2: { newBulan = 'Februari'; break; }
  case 3: { newBulan = 'Maret'; break; }
  case 4: { newBulan = 'April'; break; }
  case 5: { newBulan = 'Mei'; break; }
  case 6: { newBulan = 'Juni'; break; }
  case 7: { newBulan = 'Juli'; break; }
  case 8: { newBulan = 'Agustus'; break; }
  case 9: { newBulan = 'September'; break; }
  case 10: { newBulan = 'Oktober'; break; }
  case 11: { newBulan = 'November'; break; }
  case 12: { newBulan = 'Desember'; break; }
  default: break;
}

let str = `${tanggal} ${newBulan} ${tahun}`
console.log(str);