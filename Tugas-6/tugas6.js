// Soal 1
const luasLingkaran = (jari) => {
  let phi = 22/7
  return phi * (jari * jari)
}

const kelilingLingkaran = (jari) => {
  let phi = 22/7
  return 2 * phi * jari
}

console.log(luasLingkaran(14));
console.log(kelilingLingkaran(14));


// Soal 2
let kalimat = ''
const tambahKata = (kata) => {
  if(kalimat === ''){
    return kalimat = kata
  } else {
    return kalimat = `${kalimat} ${kata}`
  }
}
tambahKata('saya')
tambahKata('adalah')
tambahKata('seorang')
tambahKata('frontend')
tambahKata('developer')
console.log(kalimat);


// Soal 3
const newFunction = (firstName, lastName) => {
  return { firstName, lastName,
    fullName: () => {
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 


// Soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject

console.log(firstName, lastName, destination, occupation)


// Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)