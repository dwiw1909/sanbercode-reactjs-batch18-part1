var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let time = 10000
readBooksPromise(time, books[0])
.then((result) => {
  readBooksPromise(result, books[1])
   .then((result1) => {
      readBooksPromise(result1, books[2])
      .then((result2) => {
      console.log(`semua buku sudah terbaca, sisa waktu ${result2}`);
      })
   })
})