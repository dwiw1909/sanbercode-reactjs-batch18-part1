// SOAL 1
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let time = 10000

readBooks(10000, books[0], (sisa) => {
  if(sisa !== 0){
    readBooks(sisa, books[1], (sisa2) => {
      if(sisa2 != 0){
        readBooks(sisa2, books[2], (sisa3) => {
          if(sisa3 !== 0){
            readBooks(sisa3, books[3], (sisa4) => {
              console.log(`semua buku sudah terbaca, sisa waktu ${sisa4}`);
            })
          }
        })
      }
    })
  }
})
