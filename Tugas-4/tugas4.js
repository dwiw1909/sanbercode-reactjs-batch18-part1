// Soal 1
let plus = true
let nilai = 1
while(nilai > 0) {
  if (plus && nilai < 2){
    console.log('LOOPING PERTAMA');
    nilai++
  } else if (plus && nilai > 20){
    console.log('LOOPING KEDUA');
    plus = false
    nilai-=2
  } else if (plus && nilai <= 20){
    console.log(nilai + ' - I love coding');
    nilai+=2
  } else if (!plus){
    console.log(nilai + ' - I will become a frontend developer');
    nilai-=2
  }
}

// Soal 2
for (let i = 1; i <= 20; i++) {
  if(i % 3 === 0 && i % 2 !== 0){
    console.log(i + ' - I Love Coding');
  } else if(i % 2 === 0){
    console.log(i + ' - Berkualitas');
  } else {
    console.log(i + ' - Santai');
  }
}

// Soal 3
for(let i = 1; i <= 7; i++){
  let temp = ''
  for(let j = i; j > 0; j--){
    temp += '#'
  }
  console.log(temp);
}

// soal 4
var kalimat="saya sangat senang belajar javascript"
let arr = kalimat.split(' ')
console.log(arr);

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
let newArr = daftarBuah.sort()
for(let i = 0; i < newArr.length; i++){
  console.log(newArr[i]);
}